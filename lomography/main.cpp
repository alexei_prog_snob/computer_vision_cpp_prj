#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <functional>


const cv::String keys = 
"{help h usage ? |            | print this message}"
"{path           |../test_imgs| path to file      }"
"{file           |lena.jpg    | image name        }";

int main(int argc, char** argv) {
    cv::CommandLineParser parser(argc, argv, keys);
    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }
    // read image
    cv::String imgPath = parser.get<cv::String>("path") + "/" + parser.get<cv::String>("file");
    cv::Mat img = cv::imread(imgPath);
    if (img.empty()) {
        std::cout << "fail to open an image " << imgPath << std::endl;
        return -1;
    }

    cv::namedWindow("Original");
    cv::imshow("Original", img);

    cv::Mat result;

    // Create Look-up table for color curve effect
    const double expenetialE = std::exp(1.0);
    cv::Mat lut(1, 256, CV_8UC1);
    for (uint32_t i = 0; i < 256; ++i) {
        float x = static_cast<float>(i) / 256.0;
        lut.at<uchar>(i) = cvRound(256 * (1 / (1 + cv::pow(expenetialE, -((x-0.5)/0.1)))));
    }

    // Split the image channels and apply curve transform only to red channel
    std::vector<cv::Mat> bgr;
    cv::split(img, bgr);
    cv::LUT(bgr[2], lut, bgr[2]);
    cv::merge(bgr, result);
    cv::namedWindow("After curve transform");
    cv::imshow("After curve transform", result);

    // Create image for halo dark
    cv::Mat halo(img.rows, img.cols, CV_32FC3, cv::Scalar(0.3, 0.3, 0.3));
    cv::namedWindow("halo dark");
    cv::imshow("halo dark", halo);

    // Create Create circle
    cv::Point circleCenter(img.cols / 2, img.rows / 2);
    int circleRadius = img.cols / 3;
    cv::circle(halo, circleCenter, circleRadius, cv::Scalar(1, 1, 1), -1);
    cv::namedWindow("halo circle");
    cv::imshow("halo circle", halo);

    cv::Size blurKernel(img.cols / 3, img.rows / 3);
    cv::blur(halo, halo, blurKernel);
    cv::namedWindow("halo circle blurred");
    cv::imshow("halo circle blurred", halo);

    // Convert the result to float to allow multiply by factor
    cv::Mat resultf;
    result.convertTo(resultf, CV_32FC3);
    cv::namedWindow("resultf");
    cv::imshow("resultf", resultf);
    
    // Multiply our result with halo
    cv::multiply(resultf, halo, resultf);
    cv::namedWindow("resultf multiply");
    cv::imshow("resultf multiply", resultf);

    // Convert to 8 bits
    resultf.convertTo(result, CV_8UC3);

    // Show result
    cv::namedWindow("Final");
    cv::imshow("Final", result);

    cv::waitKey(0);
    cv::destroyAllWindows();
    return 0;
}