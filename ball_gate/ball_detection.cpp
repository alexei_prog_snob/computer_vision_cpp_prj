#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp> // cv::GaussianBlur
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <chrono> 
#include <string>

using Contour = std::vector<cv::Point>;
using ContourArray = std::vector<Contour >;
static uint32_t _GetMaxAreaContour(const ContourArray& _contours) {
    double maxArea = 0;
    uint32_t idx = _contours.size();
    for (uint32_t i = 0; i < _contours.size(); ++i) {
        double currentArea = cv::contourArea(_contours.at(i));
        if (currentArea > maxArea) {
            maxArea = currentArea;
            idx = i; 
        }
    }
    return idx;
}

int main() {
    cv::Mat ballImage = cv::imread("../test_imgs/robocup_ball.jpg");

    if (ballImage.empty()) {
        std::cerr << "fail to open a ball image" << std::endl;
        return -1;
    }

    // convert to HSV
    cv::Mat hsvImg = ballImage;
    cv::cvtColor(hsvImg, hsvImg, cv::COLOR_BGR2HSV);

    // blurr image
    // data structure is the fact the memory block is only copied when if explicitly reqysested
    cv::Mat blurred = hsvImg;
    cv::Size blurKernel(11, 11);
    cv::GaussianBlur(blurred, blurred, blurKernel, 0, 0);

    // convert to binary image
    cv::Scalar lowwer(5, hsvImg, 130);
    cv::Scalar upper(20, 255, 255);
    cv::Mat mask = blurred;
    cv::inRange(mask, lowwer, upper, mask);

    auto erodeData = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(4, 4));
    cv::Mat erodedDilated = mask;
    cv::erode(erodedDilated, erodedDilated, erodeData);
    cv::dilate(erodedDilated, erodedDilated, erodeData);
    
    std::vector<cv::Vec3f> circles;
    cv::HoughCircles(erodedDilated, circles, cv::HOUGH_GRADIENT, 1, erodedDilated.rows / 8, 20, 3, 0, 100);
    
    cv::Point2f center;
    float radius;
    if (circles.empty() == false) {
        ContourArray contours;
        cv::findContours(erodedDilated, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
        if (contours.size() > 0) {
            Contour ball = contours.at(_GetMaxAreaContour(contours));
            cv::minEnclosingCircle(ball, center, radius);
        }
    }
    
    cv::Mat original = cv::imread("robocup_ball.jpg");
    cv::circle(original, center, radius, cv::Scalar(255,255,255), cv::LINE_4, 1, 0);
    cv::namedWindow("Final");
    cv::imshow("Final", original);
    cv::waitKey(0); // wait for esc
    return 0;
}
