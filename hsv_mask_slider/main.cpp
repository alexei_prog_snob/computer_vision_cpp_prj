#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <functional>

/******************************************************* Mask *******************************************************/

const std::vector<std::string> values = {
    "lowwer",
    "upper"
};

const std::vector<std::string> hsvValues = {
    "Hue",
    "Saturation",
    "Value"
};

struct TrackData {
    cv::Mat m_img;
    std::map<std::string, int> m_trackbarVariableMap;
};

void GetScalars(cv::Scalar& _upper, cv::Scalar& _lowwer, std::map<std::string, int>& _trackbarVariableMap) {
    for (const std::string& value : values) {
        uint8_t place = 0;
        for (const std::string& hsvValue : hsvValues) {
            std::string tag = value + " " + hsvValue;
            if ("lowwer" == value) {
                _lowwer[place] = _trackbarVariableMap[tag];
            } else {
                _upper[place] = _trackbarVariableMap[tag];
            }
            ++place;
        }
    }
}


void ChangeMask(int , void *_userdata) {
    TrackData* trackData = reinterpret_cast<TrackData*>(_userdata);
    cv::Mat mask;
    cv::Scalar lowwer(0, 0, 0);
    cv::Scalar upper(0, 0, 0);
    GetScalars(upper, lowwer, trackData->m_trackbarVariableMap);
    cv::inRange(trackData->m_img, lowwer, upper, mask);

    cv::imshow("Image HSV Mask", mask);
}
/******************************************************* ************************************************************/


/******************************************************* Hist *******************************************************/
// typedef void callback(int, void *);
void _ShowHistoCallback(int _statew, void *_userData) {
    cv::Mat* img = reinterpret_cast<cv::Mat*>(_userData);
    // Separate image in BRG
    std::vector<cv::Mat> bgr;
    cv::split(img, bgr);
    
    // Create the histogram for 256 bins
    // The number of possibles values [0...255]
    int numBins = 256;

    // Set the ranges for B,G,R last is not included
    float range[] = {0, 256};
    const float* histRange = {range};

    cv::Mat bHist;
    cv::Mat gHist;
    cv::Mat rHist;


    // void cv::calcHist	(	
    //      const Mat       *images,
    //      int 	        nimages,
    //      const int       *channels,
    //      InputArray 	    mask,
    //      OutputArray 	hist,
    //      int 	        dims,
    //      const int       *histSize,
    //      const float     **ranges,
    //      bool 	        uniform = true,
    //      bool 	        accumulate = false 
    // )	
    cv::calcHist(&bgr[0], 1, 0, cv::Mat(), bHist, 1, &numBins, &histRange);
    cv::calcHist(&bgr[1], 1, 0, cv::Mat(), gHist, 1, &numBins, &histRange);
    cv::calcHist(&bgr[2], 1, 0, cv::Mat(), rHist, 1, &numBins, &histRange);
}

void _EqualizeCallback(int, void *) {
}

void _LomoCallback(int _state, void *_userData) {
}

void _CartoonCallback(int, void *) {
}

const std::map<std::string, cv::ButtonCallback> histButtons {
    {"Show histogran", _ShowHistoCallback},
    {"Equalize histogran", _EqualizeCallback},
    {"Lomography effect", _LomoCallback},
    {"Cartoonize effect", _CartoonCallback}
};

// const std::map<std::string, int> histButtons {
//     {"Show histogran", 1},
//     {"Equalize histogran", 2},
//     {"Lomography effect", 3},
//     {"Cartoonize effect", 4}
// };

/********************************************************************************************************************/
const cv::String keys = 
"{help h usage ? |            | print this message}"
"{path           |../test_imgs| path to file      }"
"{file           |lena.jpg    | image name        }";

int main(int argc, char** argv) {
    cv::CommandLineParser parser(argc, argv, keys);
    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }
    // read image
    cv::String imgPath = parser.get<cv::String>("path") + "/" + parser.get<cv::String>("file");
    cv::Mat img = cv::imread(imgPath);
    if (img.empty()) {
        std::cout << "fail to open an image " << imgPath << std::endl;
        return -1;
    }

    // print image
    cv::namedWindow("Original");

    for (const std::pair<const std::string, cv::ButtonCallback>& button : histButtons) {
        cv::createButton(button.first, button.second, nullptr, cv::QT_PUSH_BUTTON, 0);
    }

    cv::imshow("Original", img);

    // convert to HSV
    cv::Mat hsvImg;
    cv::cvtColor(img, hsvImg, cv::COLOR_BGR2HSV);

    // Define the User data for trackbar
    TrackData trackData;

    // Blur the hsv image and store it in trackbar data
    cv::Size blurKernel(11, 11);
    cv::GaussianBlur(hsvImg, trackData.m_img, blurKernel, 0, 0);

    // define the window for trackbar
    const std::string windowName = "Image HSV Mask";
    cv::namedWindow(windowName);


    // define all trackbar
    for (const std::string& value : values) {
        for (const std::string& hsvValue : hsvValues) {
            std::string newTrackbarName = value + " " + hsvValue;
            trackData.m_trackbarVariableMap[newTrackbarName]  = 0; // create default value

            cv::createTrackbar(
                newTrackbarName, // Name of the created trackbar.
                windowName, // Name of the window that will be used as a parent of the created trackbar.

                // Optional pointer to an integer variable whose value reflects 
                // the position of the slider. Upon creation, the slider position is defined by this variable.
                &trackData.m_trackbarVariableMap[newTrackbarName], 
                
                255, // Maximal position of the slider. The minimal position is always 0.

                // Pointer to the function to be called every time the slider changes position. T
                // his function should be prototyped as void Foo(int,void*); , 
                // where the first parameter is the trackbar position and the second parameter is the user data (see the next parameter). 
                // If the callback is the NULL pointer, no callbacks are called, but only value is updated.
                ChangeMask,

                // User data that is passed as is to the callback. 
                // It can be used to handle trackbar events without using global variables.
                &trackData
            );
        }

    }

    // call first time to call back
    ChangeMask(0, &trackData);

    // wait for esc and close all windows
    cv::waitKey(0);
    cv::destroyAllWindows();
}

