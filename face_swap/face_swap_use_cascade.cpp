#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

// data/haarcascades/haarcascade_frontalface_alt.xml
// data/haarcascades/haarcascade_eye_tree_eyeglasses.xml
static uint32_t _FindTheMouthPlace(const std::vector<cv::Rect>& _mouths) {
    uint32_t y = 0;
    uint32_t idx = 0;
    uint32_t foundIdx = 0;
    for (const cv::Rect& mouth : _mouths) {
        if (mouth.y > y) {
            foundIdx = idx;
            y = mouth.y;
        }
        ++idx;
    }
    return foundIdx;
}

namespace aps {

class SwapFaceClass {
public:
    SwapFaceClass(const cv::Mat& _img, cv::Rect&& _faceRect)
    : m_faceRectToSwap(std::move(_faceRect)) {
        _img(_faceRect).copyTo(m_faceToSwapCroped);
    }
    
    ~SwapFaceClass() = default; 
    SwapFaceClass(SwapFaceClass&&) = default;

    // nocopyable 
    SwapFaceClass(const SwapFaceClass&) = delete;
    SwapFaceClass& operator=(const SwapFaceClass&) = delete;

    const cv::Rect& GetFaceRect() const {
        return m_faceRectToSwap;
    }

    const cv::Mat& GetFaceToSwap() const {
        return m_faceToSwapCroped;
    }

private:
    cv::Rect m_faceRectToSwap;
    cv::Mat m_faceToSwapCroped;
};

}

int main() {
    // define all cascades
    cv::CascadeClassifier faceCascade;
    cv::CascadeClassifier eyeCascade;
    cv::CascadeClassifier mouthCascade;

    // load all cacades
    if (faceCascade.load("./HaarCascades/face.xml") != true) {
        std::cout << "fail to load face xml" << std::endl;
        return -1;
    }

    if (eyeCascade.load("./HaarCascades/eye.xml") != true) {
        std::cout << "fail to load eye xml" << std::endl;
        return -1;
    }

    if (mouthCascade.load("./HaarCascades/mouth.xml") != true) {
        std::cout << "fail to load mouth xml" << std::endl;
        return -1;
    }

    // read image
    cv::Mat img = cv::imread("./test1.jpg");
    if (img.empty()) {
        std::cout << "fail to load test image" << std::endl;
        return -1;
    }

    // convert to gray
    cv::Mat grayImg;
    cv::cvtColor(img, grayImg, cv::COLOR_BGR2GRAY);
    cv::equalizeHist(grayImg, grayImg);
    
    // detect faces
    std::vector<cv::Rect> faces;
    faceCascade.detectMultiScale(grayImg, faces);
    if (faces.size() < 1) {
        std::cout << " error not detected anought faces" << std::endl;
        return -1;
    }

    std::vector<aps::SwapFaceClass> facesToSwap;

    // drow the detected faces
    for (const cv::Rect& faceRect : faces) {
        // cv::Point center( faceRect.x + faceRect.width/2, faceRect.y + faceRect.height/2 );
        // cv::ellipse(img, center, cv::Size( faceRect.width/2, faceRect.height/2 ), 0, 0, 360, cv::Scalar( 255, 0, 255 ), 4);
        
        // draw found face
        // cv::rectangle(img, faceRect, cv::Scalar(0, 255, 0));

        // Cropt face from main image
        cv::Mat faceROI = grayImg(faceRect);
        std::vector<cv::Rect> eyes;
        std::vector<cv::Rect> mouths;
        eyeCascade.detectMultiScale(faceROI, eyes);
        mouthCascade.detectMultiScale(faceROI, mouths);

        std::cout << eyes.size() << std::endl;
        std::cout << mouths.size() << std::endl;
        
        // draw found eyes
        // for (const cv::Rect& eyeRect : eyes) {
        //     cv::Rect eyeOriginalRect(
        //         eyeRect.x + faceRect.x, 
        //         eyeRect.y + faceRect.y , 
        //         eyeRect.width, 
        //         eyeRect.height);
        //     cv::rectangle(img, eyeOriginalRect, cv::Scalar(0, 255, 0));
        // }

        // lets find the mouth
        uint32_t foundIdx = _FindTheMouthPlace(mouths);
        cv::Rect mouthOriginalRect(
            mouths[foundIdx].x + faceRect.x, 
            mouths[foundIdx].y + faceRect.y, 
            mouths[foundIdx].width, 
            mouths[foundIdx].height);
        // cv::rectangle(img, mouthOriginalRect, cv::Scalar(0, 255, 0));

        cv::Rect faceToSwapRect(
            (eyes[0].x < eyes[1].x ? eyes[0].x : eyes[1].x) + faceRect.x,
            (eyes[0].y < eyes[1].y ? eyes[0].y : eyes[1].y) + faceRect.y,
            eyes[0].x < eyes[1].x ? 
                (eyes[1].x - eyes[0].x + eyes[1].width)  : 
                (eyes[0].x - eyes[1].x + eyes[0].width),
            mouths[foundIdx].y - (eyes[0].y < eyes[1].y ? eyes[0].y : eyes[0].y) + mouthOriginalRect.height
        );
        // cv::rectangle(img, faceToSwapRect, cv::Scalar(255, 255, 0));
        aps::SwapFaceClass newFace(img, std::move(faceToSwapRect));
        facesToSwap.push_back(std::move(newFace));
    }

    cv::Mat halo(img.rows, img.cols, CV_32FC3, cv::Scalar(0.3, 0.3, 0.3));
    int circleRadius = 0;
    for (uint32_t i = 0 ; i < facesToSwap.size() ; ++i) {
        uint32_t faceToPlace = i == 0 ? facesToSwap.size() - 1 : i - 1;
        const cv::Mat& imageToPlace = facesToSwap[faceToPlace].GetFaceToSwap();
        const cv::Rect& originalPlaceSize = facesToSwap[i].GetFaceRect();
        cv::Mat resizedImg;
        cv::resize(imageToPlace, resizedImg, cv::Size(originalPlaceSize.width, originalPlaceSize.height));

        cv::Mat imageROI(img, originalPlaceSize);
        cv::namedWindow("imageROI");
        cv::imshow("imageROI", imageROI);
        cv::Mat mask(resizedImg);
        cv::namedWindow("mask");
        cv::imshow("mask", mask);
        resizedImg.copyTo(imageROI, mask);
        // cv::namedWindow("Image");
        // cv::imshow("Image", img);
        // Create Create circle
        int centerHeight = originalPlaceSize.height / 2;
        int centerWidth = originalPlaceSize.width / 2;
        cv::Point circleCenter(originalPlaceSize.x  + centerWidth, originalPlaceSize.y  + centerHeight);
        circleRadius = centerHeight > centerWidth ? centerHeight : centerWidth;
        cv::circle(halo, circleCenter, circleRadius, cv::Scalar(1, 1, 1), -1);
    }

    cv::Size blurKernel(circleRadius, circleRadius);
    cv::blur(halo, halo, blurKernel);
    cv::namedWindow("halo circle ");
    cv::imshow("halo circle ", halo);
    cv::namedWindow("Image");
    cv::imshow("Image", img);

    cv::waitKey(0); // wait for esc
    return 0;
}